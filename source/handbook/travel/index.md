---
layout: markdown_page
title: "Travel"
---

## Booking Travel

### Airfare/Train

1. Send a screenshot of your flight(s) or train trip to travel@gitlab.com.

1. Once travel has been booked, you will receive a copy of your itinerary via email from the carrier.

1. Review itinerary within the first 24 hours for accuracy. If any changes are necessary, notify travel@gitlab.com.

1. If you have any special requirements, please note them in your original request.


### Accommodations

1. Fill out the [online form](https://slykahn.wufoo.com/forms/z6avvkv0oi71xf/).

1. This will send your request to travel@gitlab.com.

1. Once accommodations have been booked, you will receive a confirmation from either the accommodation or GitLab Travel depending on the vendor. 

1. Review your itinerary within the first 24 hours for accuracy. If any changes are necessary, notify travel@gitlab.com.

1. If you have any special requirements, please note them in your original request.
