---
layout: markdown_page
title: "Business Development"
---

You love talking about GitLab to people and no question or challenge is too big or small. You have experience working directly with customers in order to answer questions on getting started with a technical product. Your job is to make sure our customers are successful, from the single-shop development firm all the way up to our Fortune 500 customers, and that everyone gets the appropriate level of support.

## Responsibilities

* Work with our demand generation manager to develop the proper qualifying questions for all type of customers. Be able to identify where a customer is in the sale and marketing funnel and take the appropriate action.
* Work with our demand generation manager to follow the process from which leads pass onto sales, support, etc.
* Work with our demand generation manager to document all processes in the handbook and update as needed.
* Work closely with support to build a process where we can identify customers that would be interesting for EE and pass those onto sales.
* Manage and help inbound requests to community@gitlab.com and sales@gitlab.com.
* Work with our CMO and demand generation manager for all outbound messaging for demand campaigns.
* Work with our demand generation manager and developer marketing manager to create a signup email campaign for all users of GitLab.com. The goal of this campaign is to introduce more users to our on-premise offerings.
* Own the competitive analysis and comparison page for GitLab. Work closely with sales to identify new competitive technologies and systems and understanding the drawback and benefits.
* Work closely with our developer marketing manager to identify customer stories from all of the conversations you have with our customers.

## Requirements

* GitLab is a developer-focused open source product so an understanding of the developer product space is required.
* Is your college degree in French foreign politics with a minor in interpretive dance but you’ve been hacking together websites since you were 12? Perfect. We understand that your college degree isn’t the only thing that prepares you as a potential job candidate.
* You are obsessed with making customers happy. You know that the slightest trouble in getting started with a product can ruin customer happiness.
* Be ready to learn how to use GitLab and Git as you’ll be able to edit the website directly from terminal.
* Excellent spoken and written English
* Work remotely from anywhere in the world (Curious to see what that looks like? Check out our <a href="https://about.gitlab.com/2015/04/08/the-remote-manifesto/" target="_blank">remote manifesto</a>!)
